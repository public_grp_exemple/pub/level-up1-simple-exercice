import re


def domain_name(url):
    result = re.match("^(https?:\/\/)?(www\.)?([A-Za-zа-яА-Я\.]+)\.", url)
    return result.group(3)