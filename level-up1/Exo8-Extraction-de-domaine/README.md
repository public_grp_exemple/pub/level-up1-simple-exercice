# Exo8 récuparation de domaine

## Intro 

Ok maintenant un petit exo simple pour se détendre
On va extraire le domaine d'une url

## Consigne 

Vous l'avez compris maintenant le but du jeu est d'extraire d'une url le nom de domaine
Par exemple 
```
domain_name("http://github.com/carbonfive/raygun") == "github" 
domain_name("http://www.zombie-bites.com") == "zombie-bites"
domain_name("https://www.cnet.com") == "cnet"
```