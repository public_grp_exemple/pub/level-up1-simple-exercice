import re

def reverse_alternate(string):
    new_string = string.split()
    i=0
    while i< len(new_string):
        if (i+1)%2 ==0:
            new_string[i] = new_string[i][::-1]
            i+=1
        else:
            i+=1
    return " ".join(new_string)