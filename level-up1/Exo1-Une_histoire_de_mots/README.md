# EXERCICE DE MOTS

hello et bienvenu!! On est parti pour le premier challenge. 

## Intro 
Je sais pas si vous vous souvenez mais lorqu'on était enfant on s'amsait à compter le nombre de sylable dans un mot. Et ensuite le but était de trouver un mot avec le plus de sylables dedans. Et ben aujourd'hui on fait exactement l'inverse !


## Consigne 

Je vous demande ici de me faire une fonction qui va récupérer le plus petit mot pour contenu dans une string, voilà de quoi vous entrainer. ;) 

### Jeu de données 

Premier cas 
```
"Nous au datalab on est les meilleurs"
"Non non non et non au et puis oui finalement"
```

Donnée pour aller plus loin et vous amuser. Dans un premier temps faites fonctionner la fonciton avec la donnée si dessus. 
```
"Bon d'accord la précédente n'est pas simple"
" celle du dessus aussi "
'    mais alors    celle  la! ELle resseMBLE , vraiMEnt a rien    '
```