# Exercice de tri 

## Intro
Le tri est une chose qui peut s'avérer très complexe, on va souvent nous demander de trier de la donnée. Par exemple trier de la donnée par ordre croissante ou décroissante lors d'un appel API.

## Consigne
Pour cet exercice, je vais vous demander de faire un peu de tri dans mes vieux tableaux.

Vous allez devoir faire un tri sur un tableau, le but et de coder une fonction qui va prendre en entrer un tableau de string et qui va renvoyer un tableau de string trié par ordre, de la string la plus petite à la plus grande. 

Par exemple : ["am", "i", "alive"] deviendra ["i", "am", "alive"]