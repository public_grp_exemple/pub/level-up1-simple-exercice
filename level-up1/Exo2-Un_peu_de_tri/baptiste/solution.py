# solution in one line
def sort_by_length(arr):
    return sorted(arr, key=len)