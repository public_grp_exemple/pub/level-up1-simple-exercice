# in one line 

def is_isogram(string):
    return len(string) == len(set(string.lower()))