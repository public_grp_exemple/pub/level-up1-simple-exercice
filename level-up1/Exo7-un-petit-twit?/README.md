# Exo7 On va twitter

## Intro

On va twitter ! mais avant il nous faut une machine à nous assurer que nos twits sont correctes!

## Consignes

Vous allez donc devoir faire une fonction qui va devoir prendre en paramètre une chaine de caractère et transformer la chaine au format twitter.
La chaine de caractère doit avoir un # au début ne pas dépasser 140 caractères, ne pas contenir d'espace et avoir une majuscule au début de chaque mot.
Si la phrase dépasse les 140 caractères renvoyer False

Par exemple

```
'Hello le monde je suis un message' => '#HelloLeMondeJeSuisUnMessage'
```

## Le jeu de donnée

```
'Coucou les amis'
' Avez vous le temps pour venir '
'         Non non     Non Trop     Complexe     '
```
