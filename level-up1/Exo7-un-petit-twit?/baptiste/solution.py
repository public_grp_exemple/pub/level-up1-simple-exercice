def generate_hashtag(s):
    new_s = "".join(["#"]+s.title().split(" "))
    if len(new_s) > 140 or not s:
        return False
    else:
        return new_s