from typing import Optional

from pydantic import BaseModel

from fastapi import FastAPI, HTTPException

app = FastAPI()

class BookSchema(BaseModel):
    title: str
    author: str
    book_type: str


class Book(object):
    def __init__(self, title, author, book_type) -> None:
        self.title = title
        self.author = author
        self.book_type = book_type

    def to_dict(self):
        return {"title": self.title, "author": self.author, "book_type": self.book_type}


book_creation_list = {
    1: Book("fondation", "asimov", "science fiction"),
    2: Book("1984", "George Orwell", "science fiction"),
    3: Book("Le Meilleur des mondes", "Aldous Huxley", "science fiction"),
    4: Book("le rouge et le noir", "Stendhal", "roman"),
    5: Book("Les douze roi de: Sharakhaï", "Bradley Beaulieu", "fantastique"),
}


@app.get("/books")
def get_books():
    print('HEY')
    return {key: value.to_dict() for key, value in book_creation_list.items()}


@app.get("/books/{book_id}")
def get_book(book_id: int):
    if book_id in book_creation_list:
        return book_creation_list[book_id]

    else:
        raise HTTPException(status_code=404, detail="Item not found")


@app.delete("/books/{book_id}", status_code=204)
def delete_book(book_id: int):
    if book_id in book_creation_list:
        del book_creation_list[book_id]
        return "resource deleted successfully"

    else:
        raise HTTPException(status_code=404, detail="Item not found")


@app.put("/books/{book_id}", status_code=204)
def update_book(book_id: int, book_item:BookSchema):
    if book_id in book_creation_list:
        new_book = Book(book_item.dict()['title'], book_item.dict()['author'], book_item.dict()['book_type'])
        book_creation_list[book_id] = new_book
        return book_creation_list[book_id]

    else:
        raise HTTPException(status_code=404, detail="Item not found")

@app.post("/books", status_code=201)
def create_book(book_item: BookSchema):
    book_id = len(book_creation_list)+1
    new_book = Book(book_item.dict()['title'], book_item.dict()['author'], book_item.dict()['book_type'])
    book_creation_list[book_id] = new_book
    return ''