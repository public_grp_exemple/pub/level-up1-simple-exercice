# Level up 2

## Les différents types de tests

- les tests unitaires => test une petite partie du code, généralement un composant de l'application, une méthode ou une fonction. Sert à valider la qualité du code 
- les tests d'intégration => test qui vise à test l'interraction de l'ensemble des modules de l'application entre eux. 
- les tests fonctionnels => tests fait pour valider que l'application se comporte comme le souhaite le client, ces tests se basent sur le cahier des charges ou encore les spécifications fonctionnels
- les tests de non régression => S'assure qu'une nouvelle version du code n'introduit pas de régression au sein de l'application 
- les tests de performances => tests servant à tester des performances, par exemple le temps de réponse d'une application en fonction de la quantité de données envoyé
- les tests de configuration
- les tests d'installation

## Les modules de tests en python

- unittest => [lien doc](https://docs.python.org/3/library/unittest.html)
- pytest => [lien doc](https://docs.pytest.org/en/6.2.x/)
- doctest => [lien doc](https://docs.python.org/3/library/doctest.html)
- nose => [lien doc](https://nose.readthedocs.io/en/latest/)
- tox => [lien doc](https://tox.readthedocs.io/en/latest/)
- mock => [lien doc](https://docs.python.org/dev/library/unittest.mock.html)

## les liens qui m'ont servi pour cette présentation

- [liste des différents tests et leurs définition](http://www-igm.univ-mlv.fr/~dr/XPOSE2000/TesTs/SiteWeb/typestests.htm)
- [liste des modules python pour faire des tests](https://python-guide-pt-br.readthedocs.io/fr/latest/writing/tests.html)


## Les modules de tests les plus souvent rencontrés 

De façon générale en python les deux modules que vous rencontrerez le plus sont les modules unittest et pytest. Ce sont les deux principaux. 

## Trainning

Nous allons donc nous entrainer à faire des tests en python aujourd'hui. Je vous fournis une application, plus précisément une API. Vous allez devoir coder les tests qui vont permettre de vous assurer du fonctionnement de cette API. 
Pour celà je vais vous donner la liste des critères pour chaque routes. 

Cette application est un gestionnaire de livres, on va stockers, retirer, modifier et chercher des livres. 

## Les contraintes

- Vous devez utiliser un des deux modules suivants pytest ou unittest. 
- Vous devez faire un code qui correspond au spécification. 

## L'objectif

- vous devez tester la route /books en GET pour le cas ou le status code = 200
- vous devez tester la route /books en GET pour le cas ou le type de retour est bien une liste
- vous devez tester la route /books en POST pour le cas ou le status code = 201
- vous devez tester la route /books en POST pour que la route fonctionne (Que vous arrivez à créér un item)
- vous devez tester la route /books/{book_id} en GET pour le cas ou le status code = 404 si l'id n'existe pas
- vous devez tester la route /books/{book_id} en GET pour le cas ou le type de retour et bien du json et que c'est un json convertible en dictionnaire 
- vous devez tester la route /books/{book_id} en DELETE pour le cas ou le status code = 204
- vous devez tester la route /books/{book_id} en DELETE pour le cas ou le message de retour est "resource deleted successfully"
- vous devez tester la route /books/{book_id} en DELETE pour le cas ou le status code est un 404 pour un id donné qui n'est pas en base. 
- vous devez tester la route /books/{book_id} en PUT pour le cas ou le status code = 204
- vous devez tester la route /books/{book_id} en PUT pour le cas ou le message de retour est vide
- vous devez tester la route /books/{book_id} en PUT pour le cas ou le status code = 400 losrque vous rentrez de la donnée érroné pour la modification